//Напишите функцию max, которая сравнивает два числа и возвращает большее:
// function max(number1, number2) {
//     if (number1 > number2) {
//         return number1;
//     } else if (number2 > number1) {
//         return number2;
//     } else {
//         return "Numbers are the same or not numbers at all"
//     }
// }
// console.log(max(1,2));

// Напишите функцию-аналог Math.min(). Функция принимает любое количество чисел и возвращает меньшее из них:
//
// function min() {
//     let min = +Infinity;
//     for (let i = 0; i < arguments.length; i++) {
//         if (arguments[i] < min){
//             min = arguments[i];
//         }
//     }
//     return min;
// }
// console.log(min(1,2,5,7,-1,-17,12));
//
// // Изучите перебирающие методы массивов: forEach, filter, map. Создайте массив объектов users (~10 объектов),
// // каждый объект имеет поля firstname, lastname, age с разными значениями. Используя встроенные функции массивов:
// //     Отфильтруйте пользователей младше 18 лет
// // Добавьте каждому объекту поле fullName, которое является конкатенацией имени и фамилии
// // Сформируйте новый массив, который содержит только полное имя пользователей
// let arrayOfUsersObjects = [
//     {
//         firstname: "John",
//         lastname: "dog",
//         age: 13
//     }, {
//         firstname: "John1",
//         lastname: "dog1",
//         age: 17
//     }, {
//         firstname: "John2",
//         lastname: "dog2",
//         age: 33
//     }, {
//         firstname: "John3",
//         lastname: "dog3",
//         age: 43
//     }, {
//         firstname: "John4",
//         lastname: "dog4",
//         age: 53
//     }, {
//         firstname: "John5",
//         lastname: "dog5",
//         age: 63
//     }, {
//         firstname: "John6",
//         lastname: "dog6",
//         age: 73
//     }, {
//         firstname: "John7",
//         lastname: "dog7",
//         age: 83
//     }, {
//         firstname: "John8",
//         lastname: "dog8",
//         age: 93
//     }, {
//         firstname: "John9",
//         lastname: "dog9",
//         age: 103
//     }
// ]
//
// let arrFilter = arrayOfUsersObjects.filter((item) => item.age <= +18);
// console.log(arrFilter);
//
// arrayOfUsersObjects.forEach(function (item, index, array) {
//     item.fullname = item.firstname + item.lastname;
// });
// console.log(arrayOfUsersObjects);
//
// let newArray = arrayOfUsersObjects.map((item) => {
//     return {fullName: [item.fullname]}
// });
// console.log(newArray);
//
// // Напишите функцию аналог метода массива shift. Функция удаляет из переданного в параметре массива первый элемент.
//
// function shiftAnalog(array = []) {
//     let shiftAnalogInner = array.slice(1).map((item) => {
//         return item;
//     });
//     return shiftAnalogInner;
// }
//
// console.log(shiftAnalog(["a", "b", "c"]));
//
// // Напишите функцию аналог метода массива push.
// // Функция добавляет в конец переданного в параметре
// // массив произвольное количество элементов.
//
// function pushAnalog(array = []) {
//     for (let i = 0; i < arguments.length - 1; i++) {
//         array[array.length] = arguments[i+1];
//     }
//     return array;
// }
//
// console.log(pushAnalog(["a", "b", "c", "d"], "e", 1));

// Напишите функцию аналог метода Object.assign().
// Первый параметр функции - целевой объект, поля которого будут изменены или расширены.
// Остальные параметры - объекты-источники, полями которых будет расширяться целевой объект.
// var source = {firstname: 'Tom', age: 10}
// var s = extend(source, {firstname: 'John'}, {lastname: 'Doe'});
// console.log(s); // {firstname: 'John', age: 10, lastname: 'Doe'}

var source = {firstname: 'Tom', age: 10}

function extend(target){
    for (let i = 1; i < arguments.length; i++) {
        source = {...source, ...arguments[i]};
    }
    console.log(source);
}
var s = extend(source, {firstname: 'John'}, {lastname: 'Doe'});
console.log(s); // {firstname: 'John', age: 10, lastname: 'Doe'}

//Напишите функцию setComment с параметрами: date, message, author.
// Дата и текст сообщения - обязательные параметры, если какой-то из них или оба отсутствуют,
// то выполнение функции должно обрываться, а пользователю выдаваться предупреждение (alert) о том,
// что данные переданы некорректно. Параметр author - опциональный, но должна происходить проверка:
// если параметр не передан, то вместо него подставляется значение ‘Anonymous’.
// Функция распечатывает в консоле текст в формате:
// 				<имя_автора>, <дата>
// 				<текст_сообщения>
//
// function setComment(date, message, author) {
//     if ((date === undefined  || message === undefined)){
//         alert("Incorrect data, don't play with me")
//         return;
//     }
//     if (author === undefined) author = "Anonymous";
//     console.log(`%c ${date} %c ${message} \n %c ${author}`, "font-weight=700; font-size:14px", "font-weight=700; font-size:12px", "font-weight=400; font-size:13px")
// }
// setComment('2016-11-02','Everything is ok', 'John');
//
// // Используя замыкание, напишите функцию createTimer,
// // которая использует метод performance.now() для получения текущей временной
// // метки и служит для замера времени выполнения другого кода(код менять,
// // видоизменять нельзя, как написан так и должен остаться):

let timer = createTimer();
alert('!')  // код, время выполнения которого нужно измерить
alert( timer() ); // время в мкс от начала выполнения createTimer() до момента вызова timer()



//
// // Используя замыкания, создайте функцию createAdder(), которая принимает на вход любой примитивный параметр и возвращает функцию,
// //     которая добавляет к первому параметру второй. Функция работает по следующему принципу:
// function createAdder (firstSimpleParameter){
//     return function (secondSimpleParameter){
//         return firstSimpleParameter + secondSimpleParameter;
//     }
// }
//
// var hello = createAdder('Hello, ');
// alert( hello('John') ); // Hello, John
// alert( hello('Harry') ); // Hello, Harry
//
// var plus = createAdder(5);
// alert( plus(1) ); // 6
// alert( plus(5) ); // 10


