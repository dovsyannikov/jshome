// Создайте смешанный массив, например [1, 2, 3, ‘a’, ‘b’, ‘c’, ‘4’, ‘5’, ‘6’].
// Посчитайте сумму всех его чисел, включая строковые. Выведите сумму в alert.

// let array1 = [1, 2, 3, "a", "b", "c", "4", "5", "6"];
// let sum = 0;
// for (let i = 0; i < array1.length; i++) {
//     if (parseInt(array1[i])){
//         sum = sum + parseInt(array1[i]);
//     }
// }
// alert("Total of all numbers in array is: " + sum);

// Сгенерируйте массив из n случайных чисел с двумя знаками после запятой.
// Переберите массив и распечатайте в консоли значения его элементов,
// возведенные в пятую степень, не используя функцию Math.pow().

// let n = Math.floor(Math.random() * 50);
// array2 = [];
// for (let i = 0; i < n; i++) {
//     var randomnum = Math.floor(Math.random() * 50) + Math.random();
//     array2.push(randomnum.toFixed(2));
// }
// for (let i = 0; i < array2.length; i++) {
//     console.log((array2[i]**5).toFixed(2));
// }

// 	Создайте массив со значениями: ‘AngularJS’, ‘jQuery’
// 	Добавьте в начало массива значение ‘Backbone.js’
// 	Добавьте в конец массива значения ‘ReactJS’ и ‘Vue.js’
// 	Добавьте в массив значение ‘CommonJS’ вторым элементом
// 	Найдите и удалите из массива значение ‘jQuery’, выведите его в alert со словами “Это здесь лишнее”

// array3 = ["AngularJS", "jQuery"];
// array3.unshift("Backbone.js")
// array3.push("ReactJS","Vue.js");
// array3.splice(1, 0, "CommonJS");
// for (let i = 0; i < array3.length; i++) {
//     if (array3[i] === "jQuery"){
//         alert("Eto zdes lishnee: -------- >>>  " + array3.splice(i, 1));
//     }
// }

// Создайте строку с текстом ‘Как однажды Жак звонарь сломал фонарь головой’.
// Разбейте ее на массив слов, и переставьте слова в порядке ‘Как Жак звонарь однажды сломал головой фонарь’
// с помощью любых методов массива (indexOf, 	splice ...). Затем объедините элементы массива
// в строку и выведите в alert исходный и итоговый варианты.

// let str = "Как однажды Жак звонарь сломал фонарь головой";
// const array4 = str.split(" ");
// let arrayChanged = [...array4];
// arrayChanged.splice(1, 1);
// let lastWord = arrayChanged.pop();
// arrayChanged.splice(3,0, "однажды");
// arrayChanged.splice(5, 0, lastWord);
// alert("\nOriginal variant - " + array4.join(" ") + "\n\nModified variant - " + arrayChanged.join(" "));


// Создайте ассоциативный массив person, описывающий персону,
// с произвольным количеством произвольных полей. С помощью оператора in или typeof
// проверьте наличие в объекте свойства, прочитанного из prompt, и выведите его на экран.
// Если свойства нет, то добавляйте его в объект со значением, которое также запрашивается из prompt.

// let person = {
//     age: `25`,
//     location: "Ukraine",
//     weight: 50,
//     height: 178
// }
//
// let userKey = prompt("Please fill in suggested key in object");
// for (let key in person){
//     if (key === userKey){
//         console.log(key + " : " + person[key]);
//         break;
//     }  else {
//         person.userKey = prompt("This key isn't available, please add value for this key");
//         console.log(userKey + " : " + person.userKey);
//         break;
//     }
// }

// Сгенерируйте объект, описывающий модель телефона, заполнив
// все свойства значениями, прочитанными из prompt (например: brand,
//     model, resolution, color...), не используя вспомогательные переменные.
//     Добавьте этот гаджет персоне, созданной ранее.

// let person2 = {
//     age: `25`,
//     location: "Ukraine",
//     weight: 50,
//     height: 178
// }
//
// let phone = {
//     brand : [prompt("Please fill in the brand ")],
//     model : [prompt("Please fill in the model")],
//     resolution : [prompt("Please fill in the resolution")],
//     color : [prompt("Please fill in the color")]
// }
// person2.mobile = phone;
// console.log(person2);

// Создайте объект dates для хранения дат. Первая дата – текущая, new Date.
// Вторая дата – текущая дата минус 365 дней. Из prompt читается дата в формате
// yyyy-MM-dd. Проверьте, попадает ли введенная дата в диапазон дат объекта dates.

//
// let dates = {
//     current_date: new Date().getTime(),
//     current_date_minus_365_days: [new Date().getTime() - +31536000000],
//     users_time : [prompt("Введите дату в формате yyyy-MM-dd:")]
// }
// for (let key in dates){
//     if (key === dates.users_time){
//         console.log(Date.parse(dates[key]));
//     }
//     console.log(dates[key]);
// }


// let arr = [
//     {id: 1, name: "John", gender: "m", age: 24},
//     {id: 2, name: "John1", gender: "m", age: 21},
//     {id: 3, name: "John3", gender: "f", age: 12},
//     {id: 4, name: "John2", gender: "m", age: 34},
//     {id: 5, name: "John5", gender: "f", age: 14},
//     {id: 6, name: "John6", gender: "m", age: 134},
//     {id: 7, name: "John9", gender: "f", age: 224}
// ]
//
// arr.push({id: 8, name: "John10", gender: "f", age: 54}, {id: 9, name: "John11", gender: "f", age: 4});
// console.log(arr.length);
//
// let hasIDMoreTen = arr.some((item) => item.id >= 10);
// console.log(hasIDMoreTen);
//
// let userKate = arr.find((item) => {return item.name === "Kate"});
// console.log(userKate);
//
// let arrMapNames = arr.map((item, i) => {
//        return {
//            ...item,
//            age: item.age + 5
//        };
// });
// console.log(arrMapNames);


//
// let sumAge = 0;
//
// //foreach
// arr.forEach((item, i, users) => {
//    sumAge += item.age;
// });
//
// //map
// let arrMap = arr.map(() => {
//    return item.name;
// });
// console.log(arrMap);
//
// let arrMapAge = arr.map((item, i) => {
//    if (!(i % 2)) {
//        return {
//            ...item,
//            age: item.age + 5
//        };
//    }
//    return item;
// });
//
// // filter
// let arrFilter = arr.filter((item) => item.gender === `m`);
// console.log(arrFilter);
//
//
// //find
// //use if only 1 element, cause when he finds it he will stop
//
// let user = arr.find((item) => {return item.id === 5});
// console.log(user);
//
// //some
// let hasGirls = arr.some((item) => item.gender === "f");
// console.log(hasGirls);
//
// //every
// let hasBoys = arr.every((item) => item.gender === "m");
// console.log(hasBoys);
//
// //reduce
// let ages = arr.reduce((acc,item) => {
//     acc = acc + item.age;
//     return acc;
// }, 0)
//
// let oldUsers = arr.reduce((acc, item) => {
//     const user = {...item, age: item.age + 10};
//
//     if (user.age > 40) {
//         return [...acc, user]
//     }
//
//     return acc;
// }, []);
//
// let objNames = arr.reduce((acc, item) => {
//     return {
//         ...acc, [item.id]: item.name
//     }
// }, {})
//

//
// let user = {
//     firstname: "John",
//     lastname: "Sobakovich",
//     age: 40,
//     job: "Janitor"
// }
//
// let user2 = {
//     firstname: "Samantha",
//     lastname: "Kotovich",
//     age: 18,
//     job: "Product Analyst"
// };
//
// function f() {
//     console.log(this.firstname + " " + this.lastname + " " + this.age  + " - " + this.job);
// }
// f.call(user);
// f.apply(user2);
//
// let user3 = {
//     getInfoJob(company) {
//         return company + " " + this.job;
//     }
// };
// console.log(user3.getInfoJob.call(user,"DataArt"));
// console.log(user3.getInfoJob.apply(user2,["Plarium"]));

// 1. Есть массив чисел const arr = [3, 34, 6, 89, 32, 55, 14, 65]. Подсчитайте сколько чисел больше 33.
// - одно решение через forEach
// - одно решение через reduce
// const arr = [3, 34, 6, 89, 32, 55, 14, 65, 65];
//
// let count = 0;
// arr.forEach(number => {
//     if (number > 33){
//         count++;
//     }
// })
// console.log(count);
// let count2 = 0;
// let numberArr = arr.reduce((accum, number) => {
//     if (number > 33){
//         accum += 1;
//     }
//     return accum;
// }, 0);
// console.log(numberArr);

// 2. Есть массив имен const arr = ["Kate", "John", "Bob", "James", "Jack", "Riel", "Chuck", "Scott"].
//     Необходимо вывести строку "Hello John James Riel Scott".
// - одно решение через forEach
// - одно решение через reduce
//
// const arr = ["Kate", "John", "Bob", "James", "Jack", "Riel", "Chuck", "Scott"];
//
// let blank = "Hello";
// arr.forEach((name, i) => {
//     if (i % 2 !== 0) {
//         blank += " " + name;
//     }
// })
// console.log(blank);
//
// let final;
// arr.reduce((accum, name, i) => {
//     if (i % 2 !== 0) {
//         accum += " " + name;
//     }
//     if (i === (arr.length - 1)) {
//         console.log(accum);
//     }
//     return accum;
// }, "Hello")

//3. Есть массив объектов [
//   { id: 1, name: "John", gender: "m", age: 24, date: "2019-05" },
//   { id: 2, name: "Kate", gender: "f", age: 4, date: "2021-01" },
//   { id: 3, name: "Poll", gender: "m", age: 21, date: "2019-04" },
//   { id: 4, name: "Lena", gender: "f", age: 24, date: "2020-03" },
//   { id: 5, name: "Tim", gender: "m", age: 33, date: "2020-10" },
//   { id: 6, name: "Tom", gender: "m", age: 45, date: "2019-05" },
//   { id: 7, name: "Chris", gender: "m", age: 73, date: "2021-04" },
//   { id: 8, name: "Alice", gender: "f", age: 12, date: "2020-06" }
// ] необходимо сформировать массив объектов где gender === "m", и добавить каждому из этих объектов новый ключ
// fired: false

// let persons = [
//     {id: 1, name: "John", gender: "m", age: 24, date: "2019-05"},
//     {id: 2, name: "Kate", gender: "f", age: 4, date: "2021-01"},
//     {id: 3, name: "Poll", gender: "m", age: 21, date: "2019-04"},
//     {id: 4, name: "Lena", gender: "f", age: 24, date: "2020-03"},
//     {id: 5, name: "Tim", gender: "m", age: 33, date: "2020-10"},
//     {id: 6, name: "Tom", gender: "m", age: 45, date: "2019-05"},
//     {id: 7, name: "Chris", gender: "m", age: 73, date: "2021-04"},
//     {id: 8, name: "Alice", gender: "f", age: 12, date: "2020-06"}
// ]
// let modifiedPersons = persons.reduce((accum, persona) => {
//     if (persona.gender === "m"){
//         persona.fired = false;
//         accum.push(persona);
//     }
//     return accum;
// },[])
// console.log(modifiedPersons);

// 4. Массив тот же, необходимо получить такую структуру объекта
// {
//     [year]: [объекты с этим годом],
// ...
// }
// let modifiedPersons2 = persons.reduce((accum, persona) => {
//     const [year] = accum[persona.date.slice(0, 4)];
//     return accum;
// },{})
// console.log(modifiedPersons2);

// const result = persons.reduce((acc, item) => {
//     const [year] = item.date.split("-");
//     if (year in acc) {
//         return { ...acc, [year]: [...acc[year], item] };
//     }
//
//     return { ...acc, [year]: [item] };
// }, {});
// console.log(result);

//6. Есть функция .

// let obj = {
//     firstname: "Dmitriy",
//     lastname: "Ovsyannikov",
// }
// function getGreeting() {
//     console.log(`${this.firstname} ${this.lastname}`);
// }
// getGreeting.call(obj);
//
// //Создайте объект и вызовите эту функцию так чтобы мы могли увидеть приветствие.
//
// // 7. Создайте объект который хранит значение 0 в ключе и метод который увеличивает это значение на то,
// //     что было передано в аргументе метода.
// //     Создайте другой объект который хранит только значение 0 в ключе
// // - увеличьте значение в первом объекте на 10
// // - увеличьте значение во втором объекте на 20
//
// let obj2 = {
//     0 : function (increase) {
//         return increase;
//     }
// }
// delete Object.assign(obj2, {[obj2["0"](10)]: obj2[0] })[0];
// let obj3 = {
//     0 : undefined
// }
//
// console.log(obj2);


function Animal (value, age) {
    let name = value;
    this.age = age;

    this.say = function () {
        console.log("Voice");
    }
    this.run = function () {
        console.log("Running");
    }
}
function Cat(value, age) {
    Animal.call(this, value, age);
    this.addAge = function (){
        this.age += 10;
        console.log(this.age);
    }
    this.say = function () {
        console.log("Miau");
    }
    let parentRun = this.run;
    this.run = function () {
        if (prompt("Введите команду: КСКС") === "КСКС"){
            parentRun.call(this);
        }
    }
}
let cat = new Cat("Simba", 23);