// Создайте базовый класс Figure, который будет хранить координаты (x, y) и цвет фигуры.
// На базе класса Figure создайте три класса – Line, Rect, Circle, каждый из которых
// создает соответствующую фигуру. Пример создания экземпляров каждого класса и
// параметры фигур:
// var line = new Line(50, 250, 200, 200, 'red'); // x1, y1, x2, y2, color
// var circle = new Circle(120, 120, 50, 'green'); // x, y, r, color
// var rect = new Rect(260, 130, 60, 120, 'blue'); // x, y, w, h, color

function Figure(x, y, color) {
    this.x = x;
    this.y = y;
    this.color = color;
    this.draw = function () {
        console.log("I'm drawing")
    }
}

function Line(x, y, x2, y2, color) {
    Figure.call(this, x, y, color);
    this.x2 = x2;
    this.y2 = y2;
    this.draw = function () {

    }
}

let line = new Line(100, 100, 101, 300, `green`)

function Canvas(x, y, x2, y2, color) {
    Line.call(this, x, y, x2, y2, color);
    this.draw = function (id) {
        const canvas = document.querySelector(id);

        if (!canvas.getContext) {
            return;
        }
        const ctx = canvas.getContext('2d');

        // set line stroke and line width
        ctx.strokeStyle = 'red';
        ctx.lineWidth = 5;

        // draw a red line
        ctx.beginPath();
        ctx.moveTo(100, 100);
        ctx.lineTo(300, 100);
        ctx.fillRect(50, 50, 150, 100);
        ctx.strokeRect(50, 50, 150, 100);
        ctx.stroke();
    }
}
let canvas = new Canvas(100, 100, 101, 300, `green`)

canvas.draw(`#canvasId`);
// class Canvas {
//     constructor(id) {
//         this.ctx = document.getElementById(id).getContext('2d');
//     }
//     add(...figures) {
//         for (let figure of figures) {
//             figure.draw(this.ctx);
//         }
//     }
// }
// let canvas = new Canvas(`canvasId`);
// canvas.add(line);
// class Circle extends Figure {
//     constructor(x, y, r, color) {
//         super(x, y, color);
//         this.r = r;
//     }
//     draw(ctx) {
//     }
// }
//     'use strict'
//
//
//     function Figure(x1,y1,color){
//     this._x1=x1;
//     this._y1=y1;
//     this._color=color;
// }
//
//     function Line(x1,y1,x2,y2,color){
//     Figure.call(this,[x1,y1,color]);
//     this._x2=x2;
//     this._y2=y2;
// }
//     Line.prototype.draw = function(ctx){
//     ctx.beginPath();
//     ctx.moveTo(this._x1, this._y1);
//     ctx.lineTo(this._x2, this._y2);
//     ctx.strokeStyle = this._color;
//     ctx.stroke();
// }
//     function Canvas(sel){
//     let c=document.querySelector(sel)
//     this._ctx = c.getContext("2d");
//     c.height = 300;
//     c.width  = 300;
// };
//     Canvas.prototype.add = function(){
//         let ctx = this._ctx;
//     [].forEach.call(arguments, function(el) {
//     el.draw(ctx);
//
// });
// }
//     let drawArea = new Canvas('#canvasId');
//     let line = new Line(50, 50, 200, 200, 'red');
//     let line2 = new Line(250, 50, 70, 200, '#00FF00');
//     drawArea.add(line,line2);



