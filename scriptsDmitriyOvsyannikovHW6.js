// Создайте объект calculator с методами:
//     read() запрашивает prompt для двух значений и сохраняет их как свойства объекта x, y
// sum() возвращает сумму этих двух значений
// multi() возвращает произведение этих двух значений
// diff() возвращает разницу
// div() возвращает частное
//
// calculator.read();
// alert( calculator.sum() );
// alert( calculator.multi() );

// const calculator = {
//     x : 1,
//     y : 1,
//     read() {
//         this.x = prompt("Value for X");
//         this.y = prompt("Value for Y");
//     },
//     sum() {
//         return this.x + this.y;
//     },
//     multi() {
//         return +this.x * +this.y;
//     }
//     ,
//     diff() {
//         return +this.x - +this.y;
//     }
//     ,
//     div() {
//         return +this.x / +this.y;
//     }
// }

// calculator.read();
// alert( calculator.sum() );
// alert( calculator.multi() );


// Создайте объект coffeeMachine со свойством message:
//     ‘Your coffee is ready!’ и методом start(), при вызове которого – coffeeMachine.start()
// – через 3 секунды появляется окно с сообщением, записанным в свойстве объекта message.

// let coffeeMachine = {
//     message: "Your coffee is ready!",
//     start(){
//         setTimeout(function (){
//             alert(this.message);
//         }.bind(this), 3000);
//     }
// }
// coffeeMachine.start();

// Создайте объект counter с методами увеличения,
//     уменьшения значения счетчика и методом возврата текущего значения.
//     Используйте концепцию chaining для создания цепочки вызовов.
// let counter = {
//     value: 0,
//     inc : function () {
//         this.value++;
//         return this;
//     },
//     dec: function () {
//        this.value--;
//         return this;
//     },
//     getValue() {
//         return this.value;
//     }
// }
//
//     var current = counter.inc().inc().dec().inc().dec().getValue();
// alert(current); // 1

// Создайте объект с данными: x, y и методами: getSum, getDiff, getMulti, getDiv.
// Методы объекта ничего не реализуют, а только выводят в alert сообщения вида ‘1 + 1 = 2’ или ‘1 / 0 = Infinity’.
// Для расчетов все методы используют функционал ранее созданного калькулятора.



// const me = {
//     x: undefined,
//     y: undefined,
//     getSum(x, y){
//         this.x = x;
//         this.y = y;
//         return (`${this.x} + ${this.y} = ` + calculator.sum.call(this));
//     }, getDiff(x, y){
//
//     }, getMulti(x, y){
//
//     },getDiv(x, y){
//         this.x = x;
//         this.y = y;
//         return (`${this.x} / ${this.y} = ` + calculator.div.call(this));
//     },
//
// }
//
// alert(me.getSum(1, 1));
// alert(me.getDiv(1, 0));

// Есть следующий код:
    var country = {
        name: 'Ukraine',
        language: 'ukrainian',
        capital: {
            name: 'Kyiv',
            population: 2907817,
            area: 847.66
        }
    };

function format(start, end) {
    console.log(start + this.name + end);
}


// Допишите код, чтобы в консоли браузера появились строки, которые написаны в комментариях:

    format.call(/* Ваш код */); // Ukraine
format.apply(/* Ваш код */); // [Ukraine]
format.call(/* Ваш код */); // Kyiv
format.apply(/* Ваш код */); // Kyiv
format.apply(/* Ваш код */); // undefined

