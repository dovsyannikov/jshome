// Создайте смешанный массив, например [1, 2, 3, ‘a’, ‘b’, ‘c’, ‘4’, ‘5’, ‘6’].
// Посчитайте сумму всех его чисел, включая строковые. Выведите сумму в alert.

//HOMEWORK3

//
// let array1 = [1, 2, 3, "a", "b", "c", "4", "5", "6"];
// let sum = 0;
// for (let i = 0; i < array1.length; i++) {
//     if (parseInt(array1[i])){
//         sum = sum + parseInt(array1[i]);
//     }
// }
// alert("Total of all numbers in array is: " + sum);
//
// // Сгенерируйте массив из n случайных чисел с двумя знаками после запятой.
// // Переберите массив и распечатайте в консоли значения его элементов,
// // возведенные в пятую степень, не используя функцию Math.pow().
//
// let n = Math.floor(Math.random() * 50);
// array2 = [];
// for (let i = 0; i < n; i++) {
//     var randomnum = Math.floor(Math.random() * 50) + Math.random();
//     array2.push(randomnum.toFixed(2));
// }
// for (let i = 0; i < array2.length; i++) {
//     console.log((array2[i]**5).toFixed(2));
// }

// 	Создайте массив со значениями: ‘AngularJS’, ‘jQuery’
// 	Добавьте в начало массива значение ‘Backbone.js’
// 	Добавьте в конец массива значения ‘ReactJS’ и ‘Vue.js’
// 	Добавьте в массив значение ‘CommonJS’ вторым элементом
// 	Найдите и удалите из массива значение ‘jQuery’, выведите его в alert со словами “Это здесь лишнее”

// array3 = ["AngularJS", "jQuery"];
// array3.unshift("Backbone.js")
// array3.push("ReactJS","Vue.js");
// array3.splice(1, 0, "CommonJS");
// for (let i = 0; i < array3.length; i++) {
//     if (array3[i] === "jQuery"){
//         alert("Eto zdes lishnee: -------- >>>  " + array3.splice(i, 1));
//     }
// }

// Создайте строку с текстом ‘Как однажды Жак звонарь сломал фонарь головой’.
// Разбейте ее на массив слов, и переставьте слова в порядке ‘Как Жак звонарь однажды сломал головой фонарь’
// с помощью любых методов массива (indexOf, 	splice ...). Затем объедините элементы массива
// в строку и выведите в alert исходный и итоговый варианты.

// let str = "Как однажды Жак звонарь сломал фонарь головой";
// const array4 = str.split(" ");
// let arrayChanged = [...array4];
// arrayChanged.splice(1, 1);
// let lastWord = arrayChanged.pop();
// arrayChanged.splice(3,0, "однажды");
// arrayChanged.splice(5, 0, lastWord);
// alert("\nOriginal variant - " + array4.join(" ") + "\n\nModified variant - " + arrayChanged.join(" "));



// Создайте ассоциативный массив person, описывающий персону,
// с произвольным количеством произвольных полей. С помощью оператора in или typeof
// проверьте наличие в объекте свойства, прочитанного из prompt, и выведите его на экран.
// Если свойства нет, то добавляйте его в объект со значением, которое также запрашивается из prompt.

let person = {
    age: `25`,
    location: "Ukraine",
    weight: 50,
    height: 178
}

let userKey = prompt("Please fill in suggested key in object");
    if (userKey in person){
        console.log(userKey + " : " + person[userKey]);
    }  else {
        person[userKey] = prompt("This key isn't available, please add value for this key");
        console.log(userKey + " : " + person[userKey]);
    }
console.log(person);

// Сгенерируйте объект, описывающий модель телефона, заполнив
// все свойства значениями, прочитанными из prompt (например: brand,
//     model, resolution, color...), не используя вспомогательные переменные.
//     Добавьте этот гаджет персоне, созданной ранее.

//
// let phone = {
//     brand : [prompt("Please fill in the brand ")],
//     model : [prompt("Please fill in the model")],
//     resolution : [prompt("Please fill in the resolution")],
//     color : [prompt("Please fill in the color")]
// }
// person.mobile = phone;
// console.log(person);

// Создайте объект dates для хранения дат. Первая дата – текущая, new Date.
// Вторая дата – текущая дата минус 365 дней. Из prompt читается дата в формате
// yyyy-MM-dd. Проверьте, попадает ли введенная дата в диапазон дат объекта dates.


// let dates = {
//     current_date: new Date().getTime(),
//     current_date_minus_365_days: new Date().getTime() - +31536000000,
//     users_time : prompt("Введите дату в формате yyyy-MM-dd:")
// }
// let date = dates.users_time.replaceAll("-", " ").trim();
// let [first, second, third] = date.split(" ")
// let cleanUsersDayInMilliseconds = new Date();
// cleanUsersDayInMilliseconds.setFullYear(+first, +second, +third);
// if (dates.current_date > cleanUsersDayInMilliseconds && dates.current_date_minus_365_days < cleanUsersDayInMilliseconds){
//     console.log("Date is in range of current date minus 365 dates");
// } else {
//     console.log("Date is NOT in the range of current date minus 365 dates");
// }
//
// // Создайте пустой массив. В цикле до n на каждой итерации запускайте prompt для ввода любых символов,
// // полученное значение добавляйте в конец созданного массива. После выхода из цикла посчитайте сумму
// // всех чисел массива и выведите в alert полученный результат.
//
// let array = [];
// for (let i = 0; i < 10; i++) {
//     array.push(prompt("Please fill the value for an array"));
// }
// let sum2 = 0;
// for (let i = 0; i < array.length; i++) {
//     if (!isNaN(parseInt(array[i]))) {
//         sum2 = +sum2 + parseInt(array[i]);
//     }
// }
// alert("Sum of all numbers = "  + sum2);
//
// //	Используя вложенные циклы, сформируйте двумерный массив, содержащий таблицу умножения
//
// let tableDoubleArray = [[],[],[],[],[],[],[],[],[],[]];
//
// for (let i = 1; i < 10; i++) {
//     for (let j = 1; j < 11; j++) {
//         tableDoubleArray[i-1][j-1] = (i+"*"+j+"=" + (i * j));
//     }
// }
// for (let i = 0; i < tableDoubleArray.length; i++) {
//     console.log(tableDoubleArray[i]);
// }
//
//
// // Создайте структуру данных, полностью описывающую html-разметку картинки.
//
// let obj = {
//     src: "https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg 234324",
//     alt: "",
//     style: {
//         border: "1px solid #ccc",
//         width: "200"
//     }
// }

